# frozen_string_literal: true
require 'minitest/autorun'
require 'minitest/pride'
require './lib/node'

# Tests the Node class
class NodeTest < MiniTest::Test
  def test_it_creates_a_node
    node = Node.new(61, "Bill & Ted's Excellent Adventure")
    assert_instance_of Node, node
  end

  def test_left_begins_nil
    node = Node.new(61, "Bill & Ted's Excellent Adventure")
    assert_nil(node.left)
  end

  def test_right_begins_nil
    node = Node.new(61, "Bill & Ted's Excellent Adventure")
    assert_nil(node.right)
  end

  def test_it_holds_a_score
    node = Node.new(61, "Bill & Ted's Excellent Adventure")
    assert_equal 61, node.score
  end

  def test_it_holds_a_title
    node = Node.new(61, "Bill & Ted's Excellent Adventure")
    assert_equal "Bill & Ted's Excellent Adventure", node.title
  end


end
