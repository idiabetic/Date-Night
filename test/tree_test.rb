# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require './lib/node'
require './lib/tree'

# Tests the Node class
class TreeTest < MiniTest::Test
  def test_it_begins_nil
    tree = Tree.new()
    assert_nil(tree.root)
  end

  def test_it_inserts
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    assert_equal 61, tree.root.score
  end

  def test_it_inserts_left
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(16, "Johnny English")
    assert_equal 16, tree.root.left.score
  end

  def test_in_inserts_right
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(92, "Sharknado 3")
    assert_equal 92, tree.root.right.score
  end

  def test_it_includes?
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(92, "Sharknado 3")
    assert_equal true, tree.include?(92)
  end

  def test_it_finds_depth
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(92, "Sharknado 3")
    assert_equal 1, tree.depth_of(92)
  end

  def test_it_finds_depth_deeper
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(16, "Johnny English")
    tree.insert(92, "Sharknado 3")
    tree.insert(50, "Hannibal Buress: Animal Furnace")
    assert_equal 2, tree.depth_of(50)
  end

  def test_it_finds_max
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(16, "Johnny English")
    tree.insert(92, "Sharknado 3")
    tree.insert(50, "Hannibal Buress: Animal Furnace")
    expected = {"Sharknado 3" => 92}
    assert_equal expected, tree.max
  end

  def test_it_finds_min
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(16, "Johnny English")
    tree.insert(92, "Sharknado 3")
    tree.insert(50, "Hannibal Buress: Animal Furnace")
    expected = {"Johnny English"=>16}
    assert_equal expected, tree.min
  end

  def test_it_sorts_movies
    tree = Tree.new()
    tree.insert(61, "Bill & Ted's Excellent Adventure")
    tree.insert(16, "Johnny English")
    tree.insert(92, "Sharknado 3")
    tree.insert(50, "Hannibal Buress: Animal Furnace")
    expected = [{"Johnny English"=>16},
                {"Hannibal Buress: Animal Furnace"=>50},
                {"Bill & Ted's Excellent Adventure"=>61},
                {"Sharknado 3"=>92}]
    assert_equal expected, tree.sort
  end
end
