# frozen_string_literal: true

require './lib/node'

class Tree
  attr_accessor :root

  def initialize
    @root=nil
    @depth = 0
  end

  def insert(score, title)
    if @root.nil?
      @root = Node.new(score, title)
    else
      root.insert(score, title)
    end
  end

  def include?(movie=@root, score)
    if movie.score == score
      found = true
    elsif move_left(movie, score)
      @depth += 1
      include?(movie.left, score)

    elsif move_right(movie, score)
      @depth += 1
      include?(movie.right, score)
    else
      found = false
    end
  end

  def move_left(movie, score)
    movie.score > score && movie.left
  end

  def move_right(movie, score)
    movie.score < score && movie.right
  end

  def move_right_all(movie)
    movie.right
  end

  def move_left_all(movie)
    movie.left
  end

  def depth_of(value)
    @depth_of = 0
    include?(value)
    @depth
  end

  def max(movie=@root)
    if movie.right.nil?
      {movie.title => movie.score}
    else
      max(move_right_all(movie))
    end
  end

  def min(movie=@root)
    if movie.left.nil?
      {movie.title => movie.score}
    else
      min(move_left_all(movie))
    end
  end

  def sort(movie=@root)
    return [] if movie.nil?
    results = []
    results.concat sort(movie.left)
    results << {movie.title => movie.score}
    results.concat sort(movie.right)
    results
  end

end
