# frozen_string_literal: true


# Defines the node class
class Node
  attr_accessor :score, :title, :left, :right

  def initialize(score, title)
    @score = score
    @title = title
    @left = nil
    @right = nil
  end

  def insert(score, title)
    if self.score > score
        insert_left(score, title)
    elsif self.score < score
      insert_right(score, title)
    else
      "Duplicate Value Found"
    end
  end

    def insert_left(score, title)
      if @left.nil?
        @left= Node.new(score, title)
      else
        left.insert(score, title)
      end
    end

    def insert_right(score, title)
      if @right.nil?
        @right = Node.new(score, title)
      else
        right.insert(score, title)
      end
    end
end
